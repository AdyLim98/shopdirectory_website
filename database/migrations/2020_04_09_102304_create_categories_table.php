<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        
        DB::table('categories')->insert([
            ['name' => 'Amenities'],
            ['name' => 'Anchors'],
            ['name' => 'Beauty and Healthcare'],
            ['name' => 'Books'],
            ['name' => 'Convenience Store'],
            ['name' => 'Digital Lifestyle and Photography'],
            ['name' => 'Foods and Beverages'],
            ['name' => 'Footwear'],
            ['name' => 'Fitness'],
            ['name' => 'Leisure and Entertainment'],
            ['name' => 'Shirts and Fashion'],
            ['name' => 'Haircare and Salon'],
            ['name' => 'Hobbies'],
            ['name' => 'Others'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
