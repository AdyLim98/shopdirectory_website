<?php

use Illuminate\Database\Seeder;
use App\Shop;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shop::truncate();

        $shop1 = Shop::create([
            'name' => 'KFC',
            'code' => '123456',
            'description' => 'Finger licking good',
            'lotnumber' => 'F103',
            'zone' => '0',
            'floorlevel' => 'G',
            'category_id' => '7',
            'merchant_id' => '5',
            'image' => 'KFC.jpg',
        ]);

        $shop2 = Shop::create([
            'name' => 'Pizza Hut',
            'code' => '123457',
            'description' => 'Pizza the best',
            'lotnumber' => 'F101',
            'zone' => '1',
            'floorlevel' => 'F1',
            'category_id' => '7',
            'merchant_id' => '6',
            'image' => 'Pizza Hut.jpg',
        ]);

    }
}
