@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <a href="/shops" class="col-xs-3">
                        <button type="button" class="btn btn-primary btn-sm">Shop Categories</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
