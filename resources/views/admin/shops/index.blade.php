@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title text-left">Manage Shop
                        <a href="{{ route('admin.shops.create') }}" class="pull-right">
                            <button type="button" class="btn btn-primary btn-sm">Add New Shop</button>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    @if($shops->count() > 0)
                        <table class='table'>
                            <thead>
                            <tr>
                                <th scope='col'>Name</th>
                                <th scope='col'>Code</th>
                                <th scope='col'>Categories</th>
                                <th scope='col'>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shops as $shop)
                                <tr>
                                    <th><a href="{{ route('admin.shops.show', $shop->id) }}">{{ $shop->name }}</a></th>
                                    <th>{{ $shop->code }}</th>
                                    <th>{{ implode(', ', $shop->getCategory($shop->category_id)->toArray()) }}</th>
                                    <th>
                                        <div class="row">
                                            <a href="{{ route('admin.shops.edit', $shop->id) }}" class="col-xs-3">
                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                            </a>
                                            <form action="{{ route('admin.shops.destroy', $shop->id) }}" method="POST" class="col-xs-3">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                            </form>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div>No Shop In Record</div>
                    @endif
                    {{ $shops-> links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection