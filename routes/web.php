<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//user controller for admin
Route::namespace('Admin')->prefix('admin')->middleware('auth')->name('admin.')->group(function(){
    Route::resource('/users', 'UserController', ['except' => ['show', 'create', 'store']]);
});

//shop controller for admin
Route::namespace('Admin')->prefix('admin')->middleware('auth')->name('admin.')->group(function(){
    Route::resource('/shops', 'ShopController'); 
});

//shop controller for normal user
Route::resource('/shops', 'ShopsController');

//All people can search the shop
Route::get('/search','ShopsController@search');

