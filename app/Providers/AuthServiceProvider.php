<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-only', function($user) {
            if(Auth::user()->hasAnyRole('admin')) {
                return true;
            }
            return false;
        });

        Gate::define('merchant-only', function($user) {
            if(Auth::user()->hasAnyRole('merchant')) {
                return true;
            }
            return false;
        });

        //
    }
}
