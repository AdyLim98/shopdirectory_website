<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin-only', Auth::user())) {
            $shops = Shop::orderBy('created_at', 'desc')->paginate(10);
            return view('admin.shops.index')->with('shops', $shops);
        }
        return redirect()->route('login')->with('warning', 'Please login as Admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'code' => 'required|max:10',
            'description' => 'nullable|max:50',
            'lotnumber' => 'required|max:10',
            'zone' => 'required',
            'floorlevel' => 'required',
            'category' => 'required',
            'shop_image' => 'image|nullable|max:1999'
        ]);

        // Handle file upload
        if($request->hasFile('shop_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('shop_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('shop_image')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('shop_image')->storeAs('public/shop_images', $filenameToStore);
        } else {
            $filenameToStore = 'no-image-available.png';
        }

        // Create Post
        $shop = new Shop;
        $shop->name = $request->input('name');
        $shop->code = $request->input('code');
        $shop->description = $request->input('description');
        $shop->lotnumber = $request->input('lotnumber');
        $shop->zone = $request->input('zone');
        $shop->floorlevel = $request->input('floorlevel');
        $shop->category_id = $request->input('category');
        $shop->merchant_id = auth()->user()->id;
        $shop->image = $filenameToStore;
        $shop->save();

        return redirect('/admin/shops')->with('success', 'New Shop Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = Shop::find($id);
        return view('admin.shops.show')->with('shop', $shop);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::find($id);
        return view('admin.shops.edit')->with('shop', $shop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'code' => 'required|max:10',
            'description' => 'nullable|max:50',
            'lotnumber' => 'required|max:10',
            'zone' => 'required',
            'floorlevel' => 'required',
            'category' => 'required',
            'merchant_id' => 'required|numeric',
            'shop_image' => 'image|nullable|max:1999'
        ]);

        // Handle file upload
        if($request->hasFile('shop_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('shop_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('shop_image')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('shop_image')->storeAs('public/shop_images', $filenameToStore);
        } else {
            $filenameToStore = 'no-image-available.png';
        }

        // Create Post
        $shop = Shop::find($id);
        $shop->name = $request->input('name');
        $shop->code = $request->input('code');
        $shop->description = $request->input('description');
        $shop->lotnumber = $request->input('lotnumber');
        $shop->zone = $request->input('zone');
        $shop->floorlevel = $request->input('floorlevel');
        $shop->category_id = $request->input('category');
        $shop->merchant_id = $request->input('merchant_id');
        if($request->hasFile('shop_image')) {
            $shop->image = $filenameToStore;
        }
        $shop->save();

        return redirect('/admin/shops')->with('success', 'Shop Info Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Shop::find($id);
        if($shop->image != 'no-image-available.png'){
            // Delete Image
            Storage::delete('public/shop_images/'.$shop->image);
        }
        $shop->delete();
        return redirect('/admin/shops')->with('success', 'Shop Deleted');
    }
}
