<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function getOwner($id) {
        return DB::table('users')->where('id', $id)->pluck('name');
    }

    public function getCategory($id) {
        return DB::table('categories')->where('id', $id)->pluck('name');
    }
}
